def countingSort(collection):
  """
  Sorts an array of numbers
  @collection : array of numbers to be ordered
  """
  collectionCopy = collection.copy()    #making changes on a copy of the original array
  collectionLength = len(collectionCopy)
  highest = max(collectionCopy)
  aux = [0] * (highest+1)   # initializing my auxiliar array with 0's
  for i in collectionCopy:
    aux[i]=aux[i]+1     #populating my aux array with the number of times a particular element appear in the original array
  for i in range(1,highest+1):  #accumulating my aux array
    aux[i] = aux[i] + aux[i-1]
  for i in range(collectionLength-1,-1,-1):     #right to left to avoid instability
    collection[aux[collectionCopy[i]]-1] = collectionCopy[i]    #check the correct position of the element in the aux and then assign it to the original array in position
    aux[collectionCopy[i]] = aux[collectionCopy[i]]-1   #decrease 1 from the aux indicating a new position for the next element with the same value