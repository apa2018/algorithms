def quickSort(collection, start = 0, end = None):
    """
    Sorts an array of numbers
    @collection : array of numbers to be ordered
    """

    if end is None:
        end = len(collection) - 1       #treating the first call to this function that comes with no parameters other than the array to be ordered

    if start < end:                     #the base case of the recursion
        splitPoint = pivotPartition(collection, start, end)            #we can assume that the splitPoint is in place now
                                                                        
        quickSort(collection, start, splitPoint - 1)                  #calling the recursion to the left of the splitPoint 
        quickSort(collection, splitPoint + 1, end)

def pivotPartition(collection, start, end):
    pivot = collection[end]                                             #I chose always to pick the last element as the pivot
    i = start - 1

    for j in range(start, end):                                         # here we realocate elements that are greater or smaller than the pivot
        if collection[j] < pivot:
            i+=1
            collection[j], collection[i] = collection[i], collection[j]
        
    collection[i+1], collection[end] = collection[end], collection[i+1]    #we put the pivot in the correct place now

    return i+1                                                             #returning the new splitPoint
    
    