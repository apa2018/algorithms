import os

def readFile(path):
    """
    Reads a file located in the path given as arg
    @path : path to file
    @return : Array of collection
    """

    directory = os.path.dirname(__file__)
    absolutePath = os.path.join(directory, path)
    with open("{}".format(absolutePath),'r') as file:
    # this super cool statement functions as follows:
    # I'm taking each line in the loop and removing the '\n' so its a pure string and then I use this returned value and cast it to int.
    # This is possible because I'm using lambdas functions
        return [(lambda line:line)(int(line.rstrip())) for line in file]

def getNthDigit(number, digit):
  """
  Returns the nth digit of a number.
  @number : the number to be analyzed
  @digit : the nth digit (starts from right to left)
  """
  return number // 10**digit % 10

def isOrdered(collection, ascending = True):
    """
    Check if array is ordered
    @collection : array to be checked
    @ascending* : Checking method. True by default
    @return Boolean that tells if the array is ordered
    """
    if(ascending):
       return all(collection[i] <= collection[i+1] for i in range(len(collection)-1))
    else:
       return all(collection[i] >= collection[i+1] for i in range(len(collection)-1))
