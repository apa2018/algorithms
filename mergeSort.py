def mergeSort(collection):
    """
    Sorts an array of numbers
    @collection : array of numbers to be ordered
    """

    division(collection)

def division(wholeArr):

    if len(wholeArr) > 1:
        middle = len(wholeArr)//2                                    #finding the division point ('//' is the floor division)
        firstHalf = wholeArr[:middle]                                #dividing the first half of the wholeArray (':' is the slice operator([start:end]))
        secondHalf = wholeArr[middle:]                               #dividing the second half of the wholeArray
        
        division(firstHalf)                                    
        division(secondHalf)                                    

        merge(wholeArr, firstHalf, secondHalf)                       #merging the halves


def merge(wholeArr, firstHalf, secondHalf):
    
    lenWholeArr = len(wholeArr)
    lenFirstArr = len(firstHalf)
    lenSecondArr = len(secondHalf)
    i = j = k = 0
    
    while i < lenFirstArr and j < lenSecondArr: 
        if firstHalf[i] < secondHalf[j]: 
            wholeArr[k] = firstHalf[i] 
            i += 1
        else: 
            wholeArr[k] = secondHalf[j] 
            j += 1
        k += 1

# dumps the rest of the array
# assumes that it is ordered already
    while i < lenFirstArr: 
        wholeArr[k] = firstHalf[i] 
        i += 1
        k += 1
          
    while j < lenSecondArr: 
        wholeArr[k] = secondHalf[j] 
        j += 1
        k += 1