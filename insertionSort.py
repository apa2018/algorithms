def insertionSort(collection):
    """
    Sorts an array of numbers
    @collection : array of numbers to be ordered
    """
    pivot = None
    length = len(collection)
    # whenever this outer loop iterates, we assume that the unordered is getting smaller
    for i in range(1,length):
        # start with 1 because a list of a single element is always ordered
        pivot = collection[i]
        j = i-1
        #this loop deals with finding the correct position to the pivot
        while j >= 0 and collection[j] > pivot:
            collection[j+1], collection[j] = collection[j], collection [j+1]
            j = j-1
            