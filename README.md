## Setup

### This is a Python 3.5 project, so make sure we're on the same page

To run the code make sure you're on the correct branch in respect to the correct assignment (firstAssignment,secondAssignment,thirdAssignment, and so on...) and run:
```
python3 main.py
```

### Or just run this where you would like the repository to be:
```
git clone https://gitlab.com/apa2018/algorithms.git alissongalizaAPA && cd alissongalizaAPA && git checkout fourthAssignment && python3 main.py 
``` 
Here I'm cloning the repository, switching to the latest branch and running the code. 
Every new assignment I'll be dumping the stable and approved code by the professor to the master branch.

### And if you dont have colorama or even pip installed, run this:
```
git clone https://gitlab.com/apa2018/algorithms.git alissongalizaAPA && cd alissongalizaAPA && git checkout fourthAssignment && sudo apt-get install python3-pip && pip3 install colorama && python3 main.py
```
The above command clones the repository, switches to the latest branch, installs pip3 and colorama e then runs the code. **It needs sudo password to install pip3**.
*** 
## Algorithms

- [x] Insertionsort
- [x] Selectionsort
- [x] Mergesort
- [x] Quicksort
- [x] Countingsort
- [x] Radixsort
- [x] HeapSort

*** 

##  Dependencies:
### pip3:

Using pip3 as the package manager. To download pip3, check out their website: https://pip.pypa.io/en/stable/installing/ or take a look at this thread : https://stackoverflow.com/questions/6587507/how-to-install-pip-with-python-3

### Colorama: 

```
pip3 install colorama
```
