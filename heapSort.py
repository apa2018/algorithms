def heapSort(collection):
    """
    Sorts an array of numbers
    @collection : array of numbers to be ordered
    """
    currentLength = len(collection)
    maxLength = len(collection)
    buildMaxHeap(collection,currentLength,maxLength)                    #at the end of this, I can assure that the first element is the biggest
    for i in range(maxLength-1,-1,-1):
      collection[0], collection[i] = collection[i], collection[0]       #put the biggest element in the correct position
      currentLength-=1                                                  #omit the last swapped element of the list, since it is in the "ordered side"
      maxHeapify(collection,0,currentLength)                            #apply heapify from the beggining

def buildMaxHeap(collection,currentLength,maxLength):
  for i in range((maxLength//2)-1,-1,-1):                               #starting from non-leaf until the top
    maxHeapify(collection,i,currentLength)


def maxHeapify(collection,nodePosition,currentLength):
  left = 2*nodePosition + 1                                                 
  right = 2*nodePosition + 2
  biggest = nodePosition
  if left <= currentLength-1 and collection[left] > collection[biggest]:
    biggest = left
  if right <= currentLength-1 and collection[right] > collection[biggest]:
    biggest = right

  if biggest != nodePosition:                                           #Do I need to swap?
    collection[biggest], collection[nodePosition] = collection[nodePosition], collection[biggest]
    maxHeapify(collection,biggest,currentLength)                        #Is the parent still greater than the sons?

