from utils import getNthDigit

def radixSort(collection):
  """
  Sorts an array of numbers
  @collection : array of numbers to be ordered
  """apar

  maxDigits = len(str(max(collection)))     #tells me how many times I should run the sorting, ex.:[1,55,2] I'll run twice, but with [89,6,77898] it should run five times
  for i in range(maxDigits):
    countingSortModified(collection,i)

def countingSortModified(collection,digit):
  collectionCopy = collection.copy()    #making changes on a copy of the original array
  collectionLength = len(collectionCopy)
  columnList = list(map(lambda e:getNthDigit(e,digit),collectionCopy))
  highest = max(columnList)
  aux = [0] * (highest+1)       # initializing my auxiliar array with 0's
  for i in columnList:
    aux[i]=aux[i]+1      #populating my aux array with the number of times a particular element appear in the original array
  for i in range(1,highest+1):
    aux[i] = aux[i] + aux[i-1]      #accumulating my aux array
  for i in range(collectionLength-1,-1,-1):     #right to left to avoid instability
    collection[aux[columnList[i]]-1] = collectionCopy[i]
    aux[columnList[i]] = aux[columnList[i]]-1