from colorama import init, Fore, Style

import utils
from insertionSort import insertionSort
from selectionSort import selectionSort
from mergeSort import mergeSort
from quickSort import quickSort
from countingSort import countingSort
from radixSort import radixSort
from heapSort import heapSort

init()
data = utils.readFile('data.txt')
print('\n{green}Original array of values: {reset}\n{array}\n\n'.format(green=Fore.GREEN,reset=Style.RESET_ALL,array=data))

insertionResult = data.copy()
selectionResult = data.copy()
mergeResult = data.copy()
quickResult = data.copy()
countResult = data.copy()
radixResult = data.copy()
heapResult = data.copy()

insertionSort(insertionResult)
selectionSort(selectionResult)
mergeSort(mergeResult)
quickSort(quickResult)
countingSort(countResult)
radixSort(radixResult)
heapSort(heapResult)

print('{blue}Result of insertion sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=insertionResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(insertionResult), reset = Style.RESET_ALL))

print('{blue}Result of selection sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=selectionResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(selectionResult), reset = Style.RESET_ALL))

print('{blue}Result of merge sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=mergeResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(mergeResult), reset = Style.RESET_ALL))

print('{blue}Result of quick sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=quickResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(quickResult), reset = Style.RESET_ALL))

print('{blue}Result of counting sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=countResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(countResult), reset = Style.RESET_ALL))

print('{blue}Result of radix sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=radixResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(radixResult), reset = Style.RESET_ALL))

print('{blue}Result of heap sort:{reset}\n {array}'.format(blue=Fore.BLUE,reset=Style.RESET_ALL, array=heapResult))
print('{yellow}Is the array ordered?: {red}{status}{reset}\n'.format(yellow=Fore.YELLOW,red=Fore.RED,status=utils.isOrdered(heapResult), reset = Style.RESET_ALL))