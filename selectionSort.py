def selectionSort(collection):
    """
    Sorts an array of numbers
    @collection : array of numbers to be ordered
    """
    length = len(collection)
    for i in range(0, length):
        # picks next value to be ordered
        lowest = i
        for j in range(i+1, length):
            # checks if there's another value lower than itself
            lowest = j if collection[j] < collection[lowest] else lowest
        if collection[i] != collection[lowest]:
            # swap positions with the lowest value
            collection[i], collection[lowest] = collection[lowest], collection[i]